# lusb

Library built on the [libusb](https://github.com/libusb/libusb/) C library to provide access to USB devices as a C++ class.

[API](include/lusb/UsbDevice.hpp)

[Example](src/example.cpp)
