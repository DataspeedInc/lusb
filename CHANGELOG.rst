^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package lusb
^^^^^^^^^^^^^^^^^^^^^^^^^^

2.0.2 (2024-06-13)
------------------
* Fix include
* Contributors: Kevin Hallenbeck

2.0.1 (2022-02-09)
------------------
* Add README and example
* Fix cmake problems and update for best practices
* Add pkg-config build dependency
* Contributors: Kevin Hallenbeck

2.0.0 (2021-10-29)
------------------
* Initial ROS2 release
* Use std instead of boost
* Contributors: Kevin Hallenbeck
